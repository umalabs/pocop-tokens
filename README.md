# Proof of Chain of Possession (POCOP) Tokens

## Abstract

This document introduces POCOP tokens based on a nested, chained HMACs construction to provide chained authenticity and integrity protection.

## Introduction

Bearer tokens are vulnerable at rest and in transit when an attacker is able to intercept a token to illegally access private information. In order to mitigate some of the risk associated with bearer tokens, proof-of-chain-of-possession may be used to authenticate the token. Chain-of-possession is a chronological tamper-resistant record of all the possessors of the token and the changes that have been made.

## Concept

**Chained proof-of-possession (authenticity)**
```
HMAC(K3, HMAC(K2, HMAC(K1, m1)))
```
**Chained message checksum (integrity protection)**
```
HMAC(HMAC(HMAC(K1, m1), m2, m3))
```
**Chained authenticity and integrity protection**

The combination of the two HMAC constructions mentioned above resulted in hybrid chained authenticity and integrity protection schemes.
```
HMAC(HMAC(K3, HMAC(HMAC(K2, HMAC(K1, m1)), m2)), m3)
```
```
HMAC(K3, HMAC(HMAC(K2, HMAC(HMAC(K1, m1), m2)), m3))
```
These nested, chained HMACs constructions applied on tokens or cookies may be used to implement both new authorization protocols and to enhance existing ones.

**HMACs example of AS, client, RS1 and RS2 chaining**

* chained authenticity

MAC$`_{RS2}`$ = HMAC(K$`_{RS2}`$, HMAC(K$`_{RS1}`$, HMAC(K$`_{client}`$, NONCE$`_{AS}`$)))

* chained integrity protection

MAC =  HMAC(HMAC(HMAC(MAC$`_{RS2}`$, NONCE$`_{AS}`$), MAC$`_{client}`$ \|\| data$`_{client}`$), MAC$`_{RS1}`$ \|\| data$`_{RS1}`$)

* chained authenticity and integrity protection table

| Possessor | Data | MAC route | MAC data
| --- | --- | --- | --- |
| AS| NONCE$`_{AS}`$ | | MAC = HMAC(MAC$`_{RS2}`$, NONCE$`_{AS}`$)
| client | MAC$`_{client}`$ | MAC$`_{client}`$ = HMAC(K$`_{client}`$, NONCE$`_{AS}`$)
| client | data$`_{client}`$ | | MAC = HMAC(MAC, MAC$`_{client}`$ \|\| data$`_{client}`$)
| RS1 | MAC$`_{RS1}`$ | MAC$`_{RS1}`$ = HMAC(K$`_{RS1}`$, MAC$`_{client}`$)
| RS1 | data$`_{RS1}`$ | | MAC = HMAC(MAC, MAC$`_{RS1}`$ \|\| data$`_{RS1}`$)
| RS2 | | MAC$`_{RS2}`$ = HMAC(K$`_{RS2}`$, MAC$`_{RS1}`$)


